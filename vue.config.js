module.exports = {
  devServer: {
    port: 2200,
  },
  css: {
    loaderOptions: {
      scss: {
        prependData: '@import "~@/style/variable.scss"; @import "~@/style/mixins.scss";',
      },
    },
  },
}
