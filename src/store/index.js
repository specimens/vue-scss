import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    dark: false,
  },
  // getters is used for compute derived state based on store state
  // getters: {},
  mutations: {
    toggleDark(state) {
      state.dark = !state.dark
    },
  },
  actions: {
    toggleDark(ctx) {
      ctx.commit('toggleDark')
      let body = document.querySelector('body')
      if (this.state.dark) {
        body.classList.replace('theme--light', 'theme--dark')
      } else {
        body.classList.replace('theme--dark', 'theme--light')
      }
    },
  },
  // modules is used for dividing store so the store isn't bloated
  // modules: {},
})
