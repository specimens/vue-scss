import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home,
  },
  {
    path: '/vue',
    name: 'Vue',
    component: () => import('../views/Vue.vue'),
  },
  {
    path: '/scss',
    name: 'Scss',
    component: () => import('../views/Scss.vue'),
  },
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes,
})

export default router
